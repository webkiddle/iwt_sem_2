
	var data = [
		{
			id: 0,
			title: "sampleTitle",
			author : "sample 123"
		},
		{
			id: 1,
			title: "sampleTitle2",
			author : "sample 123"
		},
		{
			id: 2,
			title: "sample3",
			author : "sample"
		},
		{
			id: 3,
			title: "sampitle",
			author : "sam"
		}
	];


	function render(){

		var exist = '';
		var i = 0;
		while (i < data.length) {
			var rowData = "<tr>";
			rowData += "<td>" + data[i]['title'] + "</td>";
			rowData += "<td>" + data[i]['author'] + "</td>";
			rowData += "<td style='width:250px;text-align:center'><button class='btn-delete' onclick='deleteData("+i+")'>Delete</button></td></tr>";
			if(exist == null) {
				exist = rowData
			} else { 
				exist += rowData
			};
			i++;
		}

		
				exist += "<tr><td><input type='text' id='add_title' placeholder='Paper Title'></td>";
				exist += "<td><input type='text' id='add_author' placeholder='Author Name'></td>";
				exist += "<td style='text-align:center'><button class='btn-create' onclick='addData()'>Create</button></td></tr>";
				document.getElementById('paper_content').innerHTML = exist;
		
	}

	function addData() {
		var title = document.getElementById('add_title').value;
		var author = document.getElementById('add_author').value;
		
		if (title == '' || author == '') {
			return;
		}

		var dataTemp = {
			id: (data.length === 0) ? 0 :  data[data.length - 1]['id'] + 1,
			title: title,
			author: author
		};
		data.push(dataTemp);
		render();
	}

	function deleteData(id){
		console.log(id);
		data.splice(id,1);
		render();	
	}

	render();

