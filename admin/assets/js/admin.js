
function validateForm() {

	let isValid = true; //set default validation status

	// selecting form elements
	let email = document.getElementById('email');
	let password = document.getElementById('password');

	let errorEmail = document.getElementById('errorEmail');
	let errorPassword = document.getElementById('errorPassword');

	// email format Regex Expression
	let emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	// Checking for empty email field
	if ( email.value === '' ) {

		email.className = 'invalid'
		errorEmail.className = 'form-error';
		errorEmail.innerHTML = 'Email is required!';
		isValid = false;
	
	} else {
	
		email.className = ''
		errorEmail.className = 'form-error hidden';
	
	}

	// checking for valid email
	if( !emailFormat.test(email.value) ) {

		email.className = 'invalid'
		errorEmail.className = 'form-error';
		errorEmail.innerHTML = 'Invalid email address!';
		isValid = false;
	
	} else {
	
		email.className = ''
		errorEmail.className = 'form-error hidden';
	
	}

	// checking for empty password field
	if ( password.value === '' ) {
		password.className = 'invalid'
		errorPassword.className = 'form-error';
		errorPassword.innerHTML = 'Password is required!';
		isValid = false;
	} else {
		password.className = ''
		errorPassword.className = 'form-error hidden';
	}

	if (isValid) {
		window.open('dashboard.html','_self');
	}

	return false; // return validation status
}