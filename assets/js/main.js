// @author Banujan
function signIn() {
	var isValid = true;

	var email = document.getElementById('login_email').value;
	var password = document.getElementById('login_password').value;

	var emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if( !emailFormat.test(email) ) {
		document.getElementById('err_login_email').innerHTML = 'Invalid Email!';
		document.getElementById('err_login_email').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_login_email').className = 'error hide';
	}

	if ( email == '' ) {
		document.getElementById('err_login_email').innerHTML = 'Email Required!';
		document.getElementById('err_login_email').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_login_email').className = 'error hide';
	}

	if ( password == '' ) {
		document.getElementById('err_login_password').innerHTML = 'Password Required!';
		document.getElementById('err_login_password').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_login_password').className = 'error hide';
	}

	return isValid;
}

// @author Banujan
function signUp() {
	var isValid = true;

	var name = document.getElementById('create_name').value;
	var password = document.getElementById('create_password').value;
	var email = document.getElementById('create_email').value;
	var cpassword = document.getElementById('create_cpassword').value;

	var emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if ( name == '' ) {
		document.getElementById('error_create_name').innerHTML = 'Email Required!';
		document.getElementById('error_create_name').className = 'error';
		isValid = false;
	} else {
		document.getElementById('error_create_name').className = 'error hide';
	}

	if( !emailFormat.test(email) ) {
		document.getElementById('error_create_email').innerHTML = 'Invalid Email!';
		document.getElementById('error_create_email').className = 'error';
		isValid = false;
	} else {
		document.getElementById('error_create_email').className = 'error hide';
	}

	if ( email == '' ) {
		document.getElementById('error_create_email').innerHTML = 'Email Required!';
		document.getElementById('error_create_email').className = 'error';
		isValid = false;
	} else {
		document.getElementById('error_create_email').className = 'error hide';
	}

	if ( password == '' ) {
		document.getElementById('error_create_password').innerHTML = 'Password Required!';
		document.getElementById('error_create_password').className = 'error';
		isValid = false;
	} else {
		document.getElementById('error_create_password').className = 'error hide';
	}

	if ( password != cpassword ) {
		document.getElementById('error_create_cpassword').innerHTML = 'Passwords are not matching!';
		document.getElementById('error_create_cpassword').className = 'error';
		isValid = false;
	} else {
		document.getElementById('error_create_cpassword').className = 'error hide';
	}

	return isValid;
}

// @author Banujan
function sendPaper() {
	var isValid = true;

	var name = document.getElementById('name').value;
	var email = document.getElementById('mail').value;
	var title = document.getElementById('title').value;
	var file = document.getElementById('file').value;

	let emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if ( name == '' ) {
		document.getElementById('err_name').innerHTML = 'Name is required!';
		document.getElementById('err_name').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_name').className = 'error hide';
	}

	if ( email == '' ) {
		document.getElementById('err_mail').innerHTML = 'Mail is required!';
		document.getElementById('err_mail').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_mail').className = 'error hide';
	}

	if( !emailFormat.test(email) ) {
		document.getElementById('err_mail').innerHTML = 'Invalid Email!';
		document.getElementById('err_mail').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_mail').className = 'error hide';
	}

	if ( title == '' ) {
		document.getElementById('err_title').innerHTML = 'Title is required!';
		document.getElementById('err_title').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_title').className = 'error hide';
	}

	if ( file == '' ) {
		document.getElementById('err_file').innerHTML = 'File is required!';
		document.getElementById('err_file').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_file').className = 'error hide';
	}
	
	return isValid;
}

// @author Osanka
function sendMessage() {
	var isValid = true;

	var name = document.getElementById('name').value;
	var email = document.getElementById('mail').value;
	var subject = document.getElementById('subject').value;
	var message = document.getElementById('message').value;

	let emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if ( name == '' ) {
		document.getElementById('err_name').innerHTML = 'Name is required!';
		document.getElementById('err_name').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_name').className = 'error hide';
	}

	if ( email == '' ) {
		document.getElementById('err_mail').innerHTML = 'Mail is required!';
		document.getElementById('err_mail').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_mail').className = 'error hide';
	}

	if( !emailFormat.test(email) ) {
		document.getElementById('err_mail').innerHTML = 'Invalid Email!';
		document.getElementById('err_mail').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_mail').className = 'error hide';
	}

	if ( subject == '' ) {
		document.getElementById('err_subject').innerHTML = 'Subject is required!';
		document.getElementById('err_subject').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_subject').className = 'error hide';
	}

	if ( message == '' ) {
		document.getElementById('err_message').innerHTML = 'Message is required!';
		document.getElementById('err_message').className = 'error';
		isValid = false;
	} else {
		document.getElementById('err_message').className = 'error hide';
	}

	return isValid;
}
